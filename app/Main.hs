{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}


module Main where



import Protolude hiding (FilePath)
import qualified Lib
import Turtle.Options
import Turtle
import Control.Logging

parser :: Parser (FilePath , FilePath)
parser = (,) <$> optPath "gitlab" 'g' "gitlab token"
             <*> optPath  "target"  't' "where to fix stack.yaml"

main :: IO ()
main = withStdoutLogging do 
  (token, path) <- options "fix commits in stack.yaml" parser 
  Lib.run token path