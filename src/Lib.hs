{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Lib where

import Control.Lens
  ( filtered
  , (.~)
  , (^..)
  , (^?)
  )
import qualified Control.Lens as L
import Control.Logging (log')
import Data.Aeson.Lens
import qualified Data.Text as T
import Data.Yaml (Value (String), decodeFileThrow)
import qualified Data.Yaml as Y
import Network.Wreq (Options, defaults, getWith, header, responseBody)
import Protolude hiding (FilePath, catch, get)
import Turtle
  ( FilePath
  , encodeString
  , readTextFile
  , (</>)
  )

hasAssoc :: AsValue s => Text -> Value -> s -> Bool
hasAssoc k v x = x ^? key k == Just v

writeCommit :: AsValue t => Text -> Text -> t -> t
writeCommit k v =
  key "extra-deps" . _Array . traverse . filtered (hasAssoc "git" (String k)) . key "commit" .~ String v

type DB = [(Text, Text, Maybe Text)]

callGitLab :: FilePath -> IO Options
callGitLab tokenFile = do
  token <- fmap T.strip $ liftIO $ readTextFile tokenFile
  pure $ defaults & header "Authorization" .~ ["Bearer " <> encodeUtf8 token]

indices :: AsValue s => s -> Maybe (Text, Text, Int)
indices x = do
  ssh <- x ^? key "ssh_url_to_repo" . _String
  web <- x ^? key "http_url_to_repo" . _String
  id <- x ^? key "id" . _Integral
  pure (ssh, web, id)

newCommits :: FilePath -> [Text] -> IO (Value -> Value)
newCommits tokenFile repos = do
  opts <- callGitLab tokenFile
  result <-
    getWith
      opts
      "https://gitlab.com/api/v4/projects?membership=true&per_page=300"
  let glRepos =
        catMaybes $
          result
            ^.. responseBody
              . _Array
              . traverse
              . L.to indices
  -- debug' $ toS $ pShow glRepos
  fmap (appEndo . mconcat) $
    forM repos $ \name -> do
      let query :: (Text, Text, a) -> Bool
          query (ssh, web, _) = name == ssh || name == web
      case find query glRepos of
        Just (_, _, id) -> do
          result' <-
            getWith opts $
              "https://gitlab.com/api/v4/projects/"
                <> show id
                <> "/repository/branches?membership=true&per_page=300"

          let mains =
                result'
                  ^? responseBody
                    . _Array
                    . traverse
                    . filtered (liftA2 (||) (hasAssoc "name" (String "main")) (hasAssoc "name" (String "master")))
                    . key "commit"
                    . key "id"
                    . _String
          pure $ case mains of
            Nothing -> mempty
            Just new -> Endo $ writeCommit name new
        _ -> do
          log' $ "no gitlab repo for " <> name
          pure mempty

readStack :: FilePath -> IO Value
readStack fp = decodeFileThrow $ encodeString $ fp </> "stack.yaml"

writeStack :: FilePath -> Value -> IO ()
writeStack fp = Y.encodeFile
  do encodeString $ fp </> "stack.yaml"

readCommits :: AsValue s => s -> [Text]
readCommits y = y ^.. key "extra-deps" . _Array . traverse . key "git" . _String

run :: FilePath -> FilePath -> IO ()
run tokenFile fp = do
  log' "read stack file"
  stack <- readStack fp
  log' "read gitlab commits"
  change <- newCommits tokenFile $ readCommits stack
  log' "write stack file"
  writeStack fp $ change stack
  log' "done"

--   sh $ do
--     l <-
--       select $
--
--             . key "http_url_to_repo"
--             . _String
--     logS' "checking" l
--     u <- mkURI l
--     let q =
--           fromText
--             . T.dropEnd 4 -- drop ".git"
--             . T.intercalate "/"
--             $ u
--               ^.. uriPath
--                 . traverse
--                 . unRText
--     cd dataDir >> mktree q >> cd q
--     liftIO $
--       cloneOrPull (render u) $
--         render $
--           uriAuthority . _Right . authUserInfo
--             ?~ UserInfo username (Just password)
--             $ u

-- catch' :: forall e m r. (MonadCatch m, MonadIO m, Exception e) => m r -> ContT r m e
-- catch' = ContT . catch

-- consumeOutput :: Text -> Shell ()
-- consumeOutput f = inshellWithErr f mempty >>= either (log' . lineToText) (log' . lineToText)

-- cloneOrPull :: Text -> Text -> IO ()
-- cloneOrPull repo repoA = flip runContT pure $ do
--   catch' @ExitCode $ sh $ consumeOutput $ "git clone " <> repoA <> " ."
--   catch' @ExitCode $ sh $ consumeOutput "git pull --all "
--   warn' $ "all git ops failed for: " <> repo
